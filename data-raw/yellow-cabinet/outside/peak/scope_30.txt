ANALOG
Ch 1 Scale 5.00V/, Pos -190.00mV, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 60.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 1.93750V

HORIZONTAL
Mode Normal, Ref Center, Main Scale 50.00us/, Main Delay -400.000ns

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Pk-Pk(1), Cur 13.9V
Std Dev(1), Cur 1.594V

