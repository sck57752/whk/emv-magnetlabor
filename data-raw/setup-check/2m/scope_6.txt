ANALOG
Ch 1 Scale 5.00V/, Pos 0.0V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 1.0000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 60.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 2.00000V

HORIZONTAL
Mode Normal, Ref Center, Main Scale 100.0us/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Pk-Pk(1), Cur 14.7V
Std Dev(1), Cur 1.178V

