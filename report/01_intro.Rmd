# Einleitung {#intro}
## Problem

Im Magnetlabor kann das Einschalten von Geräten oder Raumbeleuchtung zu Störsignalen in Messungen führen. 
Auch das Bewegen von Kabeln oder Personen in der Nähe, kann Störungen erzeugen.
Um die Störanfälligkeit zu reduzieren, wurden daher bereits mehrere Kabel mit einem extra Kupfergeflecht versehen.

In dieser Arbeit soll systematisch geprüft werden, ob das Verwenden eines zusätzlichen Kupfergeflechts einen Einfluss auf die Störanfälligkeit hat und/oder ob es sinnvoller wäre alternative Kabel zu verwenden.

## Mögliche Lösungen
### Kupfergeflecht

Die vorhandenen RG-58 Koaxialkabel können zusätzlich mit einem äußeren Kupfergeflecht umhüllt werden.
Dieses kann für 7 EUR/m in der Elektronikwerkstatt bestellt werden. 
Das Anbringen des Kupfergeflechts geht schnell und ohne Löten.

TODO: *BILD VOM KABEL*

### Kabel

Eine weitere Möglichkeit wäre die Anschaffung neuer Kabel mit besserer Isolierung. 
Geeignet wären die Kabel [RG-223](https://koaxshop.de/gx2/RG--Koaxkabel-50-Ohm/RG-223-Koaxkabel.html), [RG-142](https://koaxshop.de/gx2/RG--Koaxkabel-50-Ohm/RG-142-Koaxkabel.html) oder [RG-400](https://koaxshop.de/gx2/RG--Koaxkabel-50-Ohm/RG-400-Koaxkabel.html).
diese haben ähnliche Maße wie die vorhandenen [RG-58](https://koaxshop.de/gx2/RG--Koaxkabel-50-Ohm/RG-58-Koaxkabel.html) Kabel, verfügen allerdings über ein zusätzliches versilbertes Kupfergeflecht zur besseren Schirmung.

```{r}
tidyr::tibble(
  "Kabel" = c(
    "RG-58", 
    "RG-223",
    "RG-142",
    "RG-400"
  ),
  "Durchmesser" = c(
    "5.0 mm", 
    "5.2 mm",
    "4.95 mm",
    "4.95 mm"
  ),
  "Isolation" = c(
    "PE", 
    "PE",
    "PTFE",
    "PTFE"
  ),
  "Schirmung" = c(
    "Cu Geflecht verzinnt", 
    "Doppelt Cu Geflecht versilbert",
    "Doppelt Cu Geflecht versilbert",
    "Doppelt Cu Geflecht versilbert"
  ),
  "Kapazität" = c(
    "101 pF/m",
    "101 pF/m",
    "96 pF/m",
    "96 pF/m"
  ),
  "Preis" = c(
    "0.75 EUR/m",
    "2.40 EUR/m",
    "6.75 EUR/m",
    "7.75 EUR/m"
  )
) |>
  knitr::kable()
```

Alle vier Kabel besitzen einen minimalen Biegeradius von 30cm, sowie einen Wellenwiderstand von 50 Ohm +/- 2.

\clearpage
