---
title: "Störanfälligkeit des Aufbaus"
subtitle: "Auswertung"
author: "Konstantin Schneider"
---

```{r setup, echo = FALSE}
knitr::opts_chunk$set(
  warning = FALSE,
  error = FALSE,
  message = FALSE
) 
```

```{r}
library(tidyverse)
```

# Daten einlesen

```{r}
data <-
  tibble(
    files = fs::dir_ls(
      path = here::here("data-raw/setup-check"),
      recurse = TRUE,
      type = "file",
      glob = "*.csv"
    )
  ) |>
  mutate(
    index = row_number(),
    length = basename(dirname(files)),
    measurement = map(
      .x = files,
      .f = read_csv,
      skip = 1
    )
  ) |>
  select(-files) |>
  unnest(measurement) |>
  rename(
    "seconds" = "second",
    "volts" = "Volt"
  )

data
```

```{r}
data_summary <-
  data |>
  group_by(index, length) |>
  summarise(
    sd_volts = sd(volts),
    pp_volts = abs(max(volts) - min(volts))
  ) |>
  group_by(length) |>
  summarise(
    pp_volts = mean(pp_volts)
  )

data_summary

write_rds(
  data_summary,
  here::here("data/setup-check-summary.rds")
)
```

# Plot

```{r}
max_response <-
  data_summary |>
  pull(pp_volts) |>
  max()

p_effect <-
  data_summary |>
  ggplot(
    aes(
      x = as_factor(length),
      y = pp_volts,
      group = 1
    )
  ) +
  geom_line() +
  geom_point() +
  scale_y_continuous(
    sec.axis = sec_axis(
      trans = ~ ./max_response,
      labels = scales::percent,
    )
  ) +
  labs(
    title = "Störanfälligkeit des Messaufbaus",
    x = "Kabellänge",
    y = "Spannung [V]"
  )

p_effect

fs::dir_create(here::here("output/plots"))
               
ggsave(
  filename = here::here("output/plots/setup-effekt.pdf"),
  plot = p_effect
)

write_rds(
  x = p_effect,
  file = here::here("data/plots/check-effect.rds")
)
```

# Tabellen

```{r}
tbl_summary <-
  data_summary |>
  mutate(
    pp_rel = pp_volts/max(pp_volts)
  ) |> 
  knitr::kable(
    col.names = c(
      "Kabellänge",
      "Mittlere Peak-to-Peak Amplitude",
      "Normierte Werte"
    ),
    caption = "Gemittelte Messwerte der Stoeranfaelligkeit.",
    digits = 3
  )

tbl_summary

write_rds(
  x = tbl_summary,
  file = here::here("data/tables/setup-check-summary.rds")
)
```
